#About

"Guilt" is a short 2D pixel art role playing puzzle game for Windows PC.

Created by two second year Vytautas Magnus university (VMU) students:

* Sandra Pučkoriūtė - art, story, programming

* Ieva Vaitasiūtė - concept art, story

*Used music and sounds credited in game.*


## Screenshots

![Title screen](https://i.imgur.com/RFMqqzt.png)
![Ineraction with objects](https://i.imgur.com/BHp7TUf.png)

![Moving between rooms](https://i.imgur.com/vQXLl5p.png)
![Interaction with NPCs](https://i.imgur.com/h8xEeyY.png)

![Hint to progress](https://i.imgur.com/x5oyoLz.png)
![First note](https://i.imgur.com/MXTEW7K.png)

## Tools

* RPG Maker VX Ace - game creation, programming

* Paint Tool SAI, Photoshop CC 2017 - art

* freesound.org, looperman.com, RPG Maker VX Ace - music, sounds

## Story
*"I felt guilty for many years, I didn't know how to get rid of it, until... I got the idea to tell my story to someone who would see it from another perspective. I put my story in small paper notes. I didn't want to reveal everything to easily, so I left some riddles.*

*My story started when I met a girl that soon became my best friend, but, unfortunately, it ended with a conflict. I didn't know it was that important to her...*

*Your adventure will start after finding the first note. I hope you won't hesitate to face the riddles and help me reach a happy ending."*